# Simple Live Monitor

并非最好，但是最简易的一个脚本，用于监控Youtube频道直播；依赖streamlink。

当然，streamlink支持的其他平台也能用，例如bilibili（自动获取直播标题将会失效并退回时间戳的文件名）。

## 原理
虽然Youtube每次直播的链接都是不一样的，但是却有一个通用的入口 `https://www.youtube.com/channel/<频道ID>/live` 连接到正在进行的直播。

得益于streamlink能够正确从这个地址提取到直播流，故设计一个简单的算法：如果streamlink不在运行，就尝试从上述地址提取直播流，然后保存到带有时间的文件名上；

配合crontab定时进行查房操作，就再也不怕直播不留档了。

## 使用方法
运行测试：

`./monitor.sh [直播地址] [文件名前缀]`

crontab的写法：

` * * * * * [path_to_repo]/monitor.sh [直播地址] [文件名前缀] >> [日志文件] 2>&1 ` 

例如：

`* * * * * ~/simple-live-monitor/monitor.sh https://www.youtube.com/channel/UCAr7rLi_Wn09G-XfTA07d4g/live kotone >> ~/Kotone.log 2>&1`

移除了兼容性很差的自动获取streamlink路径的代码，改为从两个常见安装目录自动设定。无法找到时会报错，此时可以将STREAMLINK_BIN的参数放在脚本之前，例如：

`* * * * * STREAMLINK_BIN=~/streamlink ~/simple-live-monitor/monitor.sh https://www.youtube.com/channel/UCAr7rLi_Wn09G-XfTA07d4g/live kotone >> ~/Kotone.log 2>&1`