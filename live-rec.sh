#!/bin/bash
TARGET="${1:?"Please specify recording URL"}"
TAG="${2}"
FLAG="/tmp/${TAG}.flag"
INTERVAL=60

checkStreamlink() {
  if [ -z "$STREAMLINK_BIN" ]; then
    SEARCH_LIST=("$(which streamlink)" "/usr/local/bin/streamlink" "$HOME/.local/bin/streamlink")
    for item in ${SEARCH_LIST[@]}; do
      [ -x "$item" ] && STREAMLINK_BIN="$item" && break
    done
    if [ -z "$STREAMLINK_BIN" ]; then
        echo "ERROR: Can't find streamlink binary" && exit 1
    fi
  fi
}

checkLive() {
  $STREAMLINK_BIN "$TARGET"
}

getFilename() {
  unset FILENAME
  if [ -z "$YOUTUBEDL_BIN" ]; then
    SEARCH_LIST=("$(which youtube-dl)" "/usr/local/bin/youtube-dl" "$HOME/.local/bin/youtube-dl")
    for item in ${SEARCH_LIST[@]}; do
      [ -x "$item" ] && YOUTUBEDL_BIN="$item" && break
    done
    if [ -x "$YOUTUBEDL_BIN" ]; then
      FILENAME="$($YOUTUBEDL_BIN --get-filename $TARGET)"
    fi
    if [ -z "$FILENAME" ]; then
      echo "WARN: Can't retrieve title, falling back to timestamp as filename."
      FILENAME="${TAG}_$(date '+%y%m%d_%H%M').ts"
    fi
  fi
}

recordStream(){
    touch "$FLAG"
    $STREAMLINK_BIN "$TARGET" best -o "$FILENAME" --hls-live-restart --force
    rm "$FLAG"
}

main() {
  checkStreamlink
  while true; do
    if [ -e "$FLAG" ]; then
      echo "Already recording. Abort." && exit 1
    else
      checkLive && getFilename && recordStream
    fi
    sleep $INTERVAL
  done
}


main
